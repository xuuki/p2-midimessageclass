//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <math.h>
#include "MidiMessage.hpp"

using namespace std;

int main()
{
 
    MidiMessage note;
    int userInput;
    int vel;
    
    std::cout << "Enter note value: ";
    std::cin >> userInput;
    
    note.setNoteNumber(userInput);

    std::cout << "you entered: ";
    
    std::cout << note.getNoteNumber() << " \n";
    
    std::cout << "Note Freq: " << note.getMidiNoteInHertz() << "\n";
    
    std::cout << "Enter velocity: ";
    
    std::cin >> vel;
    
    note.setFloatVelocity(vel);
    
    std::cout << "Vel in amp: " << note.getFloatVelocity() << "\n";
    
    MidiMessage midimessage (34, 120);
    
    std::cout << "note after second constructor: " << note.getNoteNumber() << " \n";
    std::cout << "Vel in amp for second constructor: " << note.getFloatVelocity() << "\n";
    
    
    
}
    
