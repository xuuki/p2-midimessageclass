//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Joel on 03/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <iostream>
#include <math.h>

    MidiMessage::MidiMessage()
    {
        std::cout << "Default Contructor\n";
    }
    
    MidiMessage::MidiMessage(int initialNoteNumber, int initialVelocity)
    {
        number = initialNoteNumber;
        velocity = initialVelocity;
    }
    
    MidiMessage::~MidiMessage()
    {
        std::cout << "Destructor\n";
    }
    void MidiMessage::setNoteNumber (int value)
    {
        if(value < 0)
            value = 0;
        number = value;
    }
    int MidiMessage::getNoteNumber() const
    {
        return number;
    }
    float MidiMessage::getMidiNoteInHertz() const
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    void MidiMessage::setFloatVelocity(float vel)
    {
        if(vel < 0)
            vel = 0;
        velocity = 1 / vel;
        
    }
    float MidiMessage::getFloatVelocity() const
    {
        return velocity;
    }

