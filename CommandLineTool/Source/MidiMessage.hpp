//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Joel on 03/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

class MidiMessage
{
public:
    MidiMessage();
    
    MidiMessage(int initialNoteNumber, int initialVelocity);
    
    ~MidiMessage();
    
    void setNoteNumber (int value);
    
    int getNoteNumber() const;
   
    float getMidiNoteInHertz() const;
    
    void setFloatVelocity(float vel);
    
    float getFloatVelocity() const;
    
private:
    int number, initvelo, noteNumber;
    float velocity;
};


#endif /* MidiMessage_hpp */
